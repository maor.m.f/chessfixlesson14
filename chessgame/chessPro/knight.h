#pragma once 
#include "soliders.h"


class Knight : public Solider
{
public:
	Knight(Board& board, char x, char y, bool color) : Solider(board, x, y, color) {};
	virtual bool check(std::string movement);
};