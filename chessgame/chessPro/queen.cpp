#include "queen.h"

/* function will check if the solider can walk this.
input: string movement - place 0 - x, place 1 - y.
output: if the movment is ok*/
bool Queen::check(std::string movement)
{
	return Rook(_board, _x, _y, _color).check(movement) || Bishop(_board, _x, _y, _color).check(movement);
}
