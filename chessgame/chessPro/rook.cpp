
#include "rook.h"

#define BOARD_SIZE 64
/* function will check if the solider can walk this.
input: string movement - place 0 - x, place 1 - y.
output: 0 - the movement is ok
6 - Invalid tool displacement*/
bool Rook::check(std::string movement)
{
	char newY = movement[1];
	char newX = movement[0];
	bool way = checkEmpyWay(newX, newY);
	if (newX == _x && newY != _y && way)
	{
		return true;
	}
	else if (newY == _y && newX != _x && way)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/* function will check if the way is empty
output: true - way is empty
false - somthing worng*/
bool Rook::checkEmpyWay(char dstX, char dstY)
{
	int i = 0;
	bool ok = true;
	if (dstX > _x)
	{
		for (i = _x - 'A' + 1 + 8*('8' -_y); i < dstX - 'A' + 8*('8' - _y); i ++)
		{
			if (!_board.isEmpty(i))
			{
				ok = false;
			}
		}
	}
	else if (dstX < _x)
	{
		for (i = _x - 'A' - 1 + 8 * ('8' - _y); i > dstX - 'A' + 8 * ('8' - _y); i --)
		{
			if (!_board.isEmpty(i))
			{
				ok = false;
			}
		}
	}
	else if (dstY < _y)
	{
		for (i = _x - 'A' + 8 + 8 * ('8' - _y); i < dstX - 'A' + 8 * ('8' - dstY); i+=8)
		{
			if (!_board.isEmpty(i))
			{
				ok = false;
			}
		}
	}
	else if (dstY > _y)
	{
		for (i = _x - 'A' - 8 + 8 * ('8' - _y); i > dstX - 'A' + 8 * ('8' - dstY); i-=8)
		{
			if (!_board.isEmpty(i))
			{
				ok = false;
			}
		}
	}
	return ok;
}

