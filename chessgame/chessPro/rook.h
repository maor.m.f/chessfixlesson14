#pragma once
#include "soliders.h"

class Rook : public Solider
{
public:
	Rook(Board& board, char x, char y, bool color) : Solider(board, x, y, color) {};
	virtual bool checkEmpyWay(char dstX, char dstY);
	virtual bool check(std::string movement);
};